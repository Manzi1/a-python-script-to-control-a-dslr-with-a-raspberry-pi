#!/usr/bin/python3.5

# With thanks to - Alexander Baran-Harper 
# https://www.youtube.com/watch?v=1eAYxnSU2aw
# Edited by Craig Manzi 12.06.19

from time import sleep
from datetime import datetime
from sh import gphoto2 as gp
import signal, os, subprocess

# Kill gphoto2 process that auto starts everytime the camera is connected
def killgphoto2Process():
    subprocess.run(['killall', 'gvfsd-gphoto2'])

shot_date = datetime.now().strftime("%Y-%m-%d")
shot_time = datetime.now().strftime("%H.%M.%S")
picID = "ACF-TimeLapse"
clearCommand = ["--folder", "/store_00020001/DCIM/100CANON", "-R", "--delete-all-files"]
trigger_download = ["--capture-image-and-download"]
folder_name = shot_date + picID
save_location = "/home/pi/acf-timelapse/" + shot_date

# Create save folder
def creatSaveFolder():
    try:
        os.makedirs(save_location)
    except:
        print ("Failed to create new dir, it might already exist.")
    os.chdir(save_location)

# Download images
def captureImages():
    gp(trigger_download)
    gp(clearCommand)
    sleep(3)

# Rename images
def renameFiles(ID):
    for filename in os.listdir("."):
        if len(filename) < 13:
            if filename.endswith(".JPG"):
                os.rename(filename, (shot_date + " " + shot_time + ".JPG"))
                print("Renamed JPG")
            elif filename.endswith(".CR2"):
                os.rename(filename, (shot_date + " " + shot_time + ".CR2"))
                print("Renamed CR2")

# Run
killgphoto2Process()
gp(clearCommand)
creatSaveFolder()
captureImages()
renameFiles(picID)
